import { RouterModule, Routes } from '@angular/router';
import { PostRoutes } from "./post/post.routing";

export const routes:Routes = [
  { path: ``, pathMatch: `full`, redirectTo: `/post/list` },
  ...PostRoutes,
  { path: `**`, redirectTo: `/post/list` }
];

export const Routing = RouterModule.forRoot(routes);