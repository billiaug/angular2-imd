import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';

import { IndexComponent } from './index/';
import { DetailComponent } from './detail/';
import { CreateComponent } from './create/';
import { routing } from "./post.routing";
import { ApiService } from "./service/api.service";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    routing
  ],
  declarations: [ IndexComponent, CreateComponent, DetailComponent],
  providers: [ ApiService ]
})
export class PostModule {
}

