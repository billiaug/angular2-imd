import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class ApiService {
  private apiUrl = 'https://jsonplaceholder.typicode.com/';

  constructor(
    private http: Http
  ) { }

  getPosts(){
    var url = this.apiUrl + 'posts';
    return this.http.get(url)
      .catch((error: any) => {
        if (error.status === 500) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 401) {
          return Observable.throw(new Error(error.status));
        }
        return Observable.throw(error);
      })
      .map((response: Response) => {
        // Return response.
        return response.json();
      })
  };

  getPost(id){
    var url = this.apiUrl + 'posts/' + id;
    return this.http.get(url)
      .catch((error: any) => {
        if (error.status === 500) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 401) {
          return Observable.throw(new Error(error.status));
        }
        return Observable.throw(error);
      })
      .map((response: Response) => {
        // Return response.
        return response.json();
      })
  };

  postPosts(data){
    var url = this.apiUrl + 'posts';
    return this.http.post(url, data)
      .catch((error: any) => {
        if (error.status === 500) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 401) {
          return Observable.throw(new Error(error.status));
        }
        return Observable.throw(error);
      })
      .map((response: Response) => {
        // Return response.
        return response.json();
      })
  };


}
