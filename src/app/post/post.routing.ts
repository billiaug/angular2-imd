import { Routes,  RouterModule }  from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { IndexComponent } from './index/index.component';
import { DetailComponent } from './detail/detail.component';
import { CreateComponent} from './create/create.component';


export const PostRoutes: Routes = [
  // { path: 'post', redirectTo: 'post/list', pathMatch: 'full'},
  { path: 'post/list', component: IndexComponent },
  { path: 'post/new', component: CreateComponent },
  { path: 'post/:id', component: DetailComponent },
];

export const routing: ModuleWithProviders = RouterModule.forChild(PostRoutes);
