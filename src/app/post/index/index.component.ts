import { Component, OnInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import 'rxjs/Rx';
import { ApiService } from "../service/api.service";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: [ './index.component.css' ],
  providers: [ FormBuilder, ApiService ]
})

export class IndexComponent implements OnInit {
  private loading: boolean = false;
  private date: any = new Date();
  private amount: number = 360;
  private detail: any = {};
  posts = [];

  constructor(private fb: FormBuilder,
              private ApiService: ApiService) {
  }

  ngOnInit() {
    this.getPosts();
  }

  showDetail(id) {
    this.detail.body = this.posts[ id ].body;
    this.detail.imgUrl = this.posts[ id ].img;
  }

  getPosts() {
    this.loading = true;
    this.ApiService.getPosts().subscribe(request => {
        this.posts = request;
        this.loading = false;
      },
      err => {
        console.error(err);
      }
    );
  }
}