import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import 'rxjs/Rx';
import { ApiService } from "../service/api.service";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [ FormBuilder, ApiService ]
})
export class CreateComponent implements OnInit {
  private createForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private ApiService: ApiService
  ) {
    this.initCreateForm();
  }

  ngOnInit() {
  }


  initCreateForm() {
    this.createForm = this.fb.group({
      'title': ['', Validators.required],
      'body': ['', Validators.required],
    });
  }

  onSubmit(form: any): void {
    this.postPosts(form);
  }

  postPosts(form) {
    this.ApiService.postPosts(form).subscribe(request => {
      // Redirect to detail view
      },
      err => {
        console.error(err);
      }
    );
  }
}
