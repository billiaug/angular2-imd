import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from "../service/api.service";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: [ './detail.component.css' ],
  providers: [ ApiService ]
})
export class DetailComponent implements OnInit {
  private post = {};
  constructor(
    private ApiService: ApiService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.getPost(params['id'])
    });
  }

  getPost(id) {
    this.ApiService.getPost(id).subscribe(request => {
        this.post = request;
      },
      err => {
        console.error(err);
      }
    );
  }

}
